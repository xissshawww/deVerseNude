+++
title = "About dVN"
description = ""
date = "2021-02-05"
aliases = ["about-me","contact"]
author = "xissshawww"

+++

<!-- This is a new experimental blog, focusing on sound art. -->
<!--  -->
<!-- 这是一个实验性的博客，关注声音艺术。 -->

<!-- 本站接受投稿（可以指定自己认可的开源协议），请联系 xiix at ctemplar.com -->

<!-- 自由組曲.我们（[freeformsuite.us](https://freeformsuite.us)）也接受相关投稿 -->

<!-- 你可以在[这儿](https://gitlab.com/xissshawww/deVerseNude/-/archive/master/deVerseNude-master.zip?path=content/posts)下载所有dVN的posts。 -->

---

未来的投稿：

<!-- - [BinchOutan](https://www.douban.com/people/50094222/)翻译的一篇[Yasunao Tone（刀根康尚）](https://www.discogs.com/artist/11326-Yasunao-Tone)的访谈 -->
<!-- - Geir Jenssen 的[blog](https://www.biosphere.no/)中的一篇：[Cho Oyu (8201 m), Tibet](https://www.biosphere.no/cho_oyu/cho_oyu.html) -->

- 更多的[Futurism](https://www.unknown.nu/futurism/)
- Iannis Xenakis 在 Formalized Music 中的《随机音乐》

---

裸身集会官方[网站](https://www.lesrallizesdenudes-official.com/)已经上线，本站点为 non-profit，如有侵权请联系。

---
title: '0(rphan)d(rift>) :: 音乐作为人造时间'
tags:
  - Fiction
  - Accelerationism
date: 2021-01-13 13:39:57
description: ""
---

原文来自[orphan drift archive](http://www.orphandriftarchive.com/articles/music-as-artificial-time/) 译者：晚霞

### **1999.**

0rphan Drift信号的频率与音乐相同。

它的灵感来自于数字炼金术、摩擦、黑暗的声音、精神活性的情况、莫比乌斯带、物理魔法、追踪过程、触摸、精神分裂症. 巫毒. 海洋. 机器视觉、变化、逃逸速度、物种的差异. 强度和融合。

*0rphan Drift*成立于1994年，是一个由不同创作背景的艺术家组成的团体，她们使用视频、电脑操作和摄影工艺，为现场多媒体活动、音乐视频、新技术出版物、会议和特定地点的艺术装置创作图像。它产生的作品由多种身份构建，以应对通过新技术和科学、全球权力结构和媒体场景而不断增加的复杂性和变化率的文化。我们的合作过程与*Burroughs*和*Gysin*提出的第三种思维概念有关，也与德勒兹和瓜塔里的哲学有关，因为我们的作品旨在通过回应、反馈和不同的观点，在事物之间建立新的联系。我们创作的作品中，不同的参考框架共存，并相互渗入，目的是发展我们对人类的认知。我们从根本上关注发展新的表达模式，足以传达一种混合和复杂的“真实”感。

我们的工作过程更类似于家庭录音室里的白标合作（译者注：White Label，意思是你开发了一个产品，而这个产品可以根据其他不同公司的品牌形象包装成该公司自身的产品），而不是艺术家个人的身份。我们对交互技术采取了一种刻意以反馈为导向的、非纯粹的方法。我们希望通过图像、声音和声音的重叠节奏模式来激活中枢神经系统。我们的策略是触觉和触觉。触觉空间指的是一个感知领域，在这个领域中，触觉成为所有感官中的一个操作元素，涉及到感官之间的交叉（同步感觉），也涉及到感觉、思维和记忆之间的交叉。这个作品旨在激活这个埋藏在地下的感性平面，我们觉得它的运作方式比视觉主导（镜面）感知的学习模式更加广泛和物理。我们想表明，控制论的过程并不会导致身体的消失，而是重新设计其感官反应。我们采取的立场是，许多新的科学和工程系统正在极大地改变以前对主客体关系的概念。

空间迷离(Spatial disorientation) 很重要。*Kodwo Eshun*在他的*比太阳更辉煌*(More Brilliant than the Sun) 一书中说，音乐是感觉工程的科学，是强化感觉的科学。他写道：

> 陷入声音的宇宙，是颗粒状的。雕刻在四维空间。音乐通过对触觉的转移将人体综合与虚拟化。每当声音到达皮下，它太分散，太流动，耳朵无法将之把握为连绵的声音。声音会传到皮肤上，而不是像节拍一样压过皮肤。有了光和声音，就有了一个地层，这两种元素一直在交叉。这让你可以类比很多东西，并进行变异和重组。一个动感的战斗区域(A warzone of kinaesthesia)。神经系统正在被节拍重塑，以达到一种新的状态，一种新的感觉条件。声音虚构，音乐作为外骨骼。音量和重复的感觉冲击。压力。创造一种新的声音生命体。未来的感觉是什么样子的感觉。要有被声音识别的感觉。声音是一种感觉技术。

*0rphan Drift*正在置换与音频技术相关的流程——视频的制作采用了最常应用于数字声音制作的流程，如循环、采样、反馈失真和详细的速度变化，创造出一种有节奏的模式。我们通过详细分析数字音频反馈和混响的过程、其复杂的分层和扭曲、质感和通道的相互作用，研究视频的装配模式，我们利用音频采样和混音模型，建立了一个在*0rphan Drift*成员中运行的视频制作过程。应用到可识别的图像上，这发展了一些技术，这些技术突变了人们熟悉的感觉、时间性和识别性的代码，以打破屏幕空间作为一个遥远的和纯粹的表象维度的传统。在“数字”和“模拟”的感觉之间进行切割，我们扭曲了人工和真实之间的再现的区别。

我们主要对想象和感觉之间发生的摩擦感兴趣，并将我们的作品视为这些摩擦所产生的“网络”的表现。我们通过作品的沉浸式和多层次性，将观众带入这个过程的互动回路。对我们来说，这种互动是基于触觉的，并探索皮肤和屏幕之间固有的棘手的情色关系。

同样从研究数字制作音乐的装配模式发展而来，复制在作品中运行。通过结合模拟和电脑效果，我们使类似的图像以一种基于时间的方式改变了注册信息。我们认为复制是一种技术，让我们探索事物在不同背景/环境下的变化方式，新技术不一定要产生一个同质的世界。我们的视频制作过程涉及尽可能多方面的采样，并将它们汇集成一个单一的有节奏的数据流，标志着新的观点改变了我们对自己的想象。复制和取样都让我们越来越多地聚焦于一个图像表面，并感受到它作为一个轨迹的存在（就像一个人可以跟随轨道上的一条线一样），让我们尝试体验在其中移动和离开的可能性。通过将再现折叠成复制，作为众多视点中的一个，它就不再是感知的主导形式，并允许我们以一种变革的方式与之合作。通过视频对特定意象的复制和变异，形成一种不断发展的真实存在感。

产生的视觉总是存在于声音之中。比如1997年的作品“*9006*”，就是为了表现数字美学在屏幕上的虚拟过程，延伸为屏幕空间之外的实际而又无形的物理过程，通过声音体现出来。一种迷幻的诱导性的联觉(synaesthetic) 反应和肉体的迷失。情感和身体的触发被视频和声音之间的节奏性交流所融合。它们的频率连续地匹配着。声音中的音量差异与视觉中的强度变化相匹配。一种空间绑架(spatial abduction) 和感官实验的感觉被提出。

愈来愈深入抽象过程的旅程与影像的不断突变相结合，使影像空间成为一个流动的、令人着迷的空间，消解了投影在实体墙上的感觉。“*9006*”将数字音乐不仅在节奏上，而且在情感和质感上所创造的空间进行了视觉化和追踪。通常在广播图像中看不见的机器过程（即反馈、静电、磁场等），在屏幕上的表现图像中表现为一个不可分割的部分。这些振动场，通常被限制在声音想象中，引发视觉导致的亲密接触的感觉。同步感觉而不是音频同步。“*9006*”流质般的颤抖着的难以辨认的意象，用表面的诱惑编织了一个分子概念上的和隐秘的视角：转化出声波振动的本质。

数字音乐美学对我们来说是如此的一种非线性时间。我们称之为“人工时间”。我们的虚构总是探索通常由数字声音艺术家表达的速度或音高和时间扭曲。经历过的时间完全被节奏、复杂性和反馈所重新编码。我们的视频提供了某种替代版本或“实时模拟”的人工时间，观众感觉同时分布在多个速度和方向上，身体上保持着无缝拼接和耗尽时间的可能性。我们试图将视频的时间性推向极端，不断地将模拟和数字视频部分相互输入，以在它们不同的时间寄存器之间产生摩擦。我们也通过图像来感受时间的层次，其色调在熟悉的图像中揭示了抽象的东西，反之亦然。液态的表面在物质上传达了这些强度，同时总是在一个你永远无法进入的时间框架中：这又是一种音频策略。这些时间框架是通过时间拉伸和循环——时间的数字复制——部分创造出来的。这种效果在视觉和声音上同时呈现，可以让观众在一种迷醉的共鸣感觉中暂停。不确定性和后像(afterimage)。次物理和超物理。在这里，线性的时间分崩离析。

变异的图像引发了关于美丽和极端的虚构的想法，这些虚构是由我们从新技术中获得的视觉所产生的。我们感兴趣的是唤起人类不一定能控制的主体性的方面，即表达一种对进化的坚持和强烈的关注，我们觉得这种关注塑造了当代生活。我们所说的进化是指：进化是变化的不可控性，进化位于具体和想象之间的关系的不稳定，进化被映射为仪式（进化的改装，通常是强化，行为模式，以帮助沟通）。

这个问题是感知的进化。是适应的问题。一种想象力的技术，设计人类与机器化过程的融合。一个宇宙，不是这个宇宙有贸易路线了。集体无意识的软件在你的故事中产生自己。深入体内的机器记忆，从不同的距离感受DNA。振动从它的源头弯曲出来。吞噬你的神经系统。最深层的干扰形式。

我们正在探索对技术和生物突变的看法。我们的项目源于一种独特的控制论文化视角，它对普遍认为赛博空间只存在于计算机系统和互联网中的假设提出了质疑。在计算机系统内部产生的“赛博空间”，在其所谓的黑盒子之外，还有许多平行的表现形式。控制论环境本质上是互动的、变革的，需要对空间、时间和施动者进行重新配置。我们想让人们看到，在人类对环境的反应中，虚拟性一直都在参与。从我们的角度来看，新技术解锁了已经存在的感知领域，这些领域有时被归入潜意识，有时则以物理抽象的方式影响。视觉在虚拟现实和音乐中发现其物理性。触觉的感染。触觉的沉浸。通过触觉视觉设计的艺术；一种同时触及每个感官的多媒体体验。没有超然的“别处”了。所有的一切都在移动，很明显，拥挤着改变我们对真实的概念。实际已经扩展到进入第四维度。音乐正在构建神经网络，绕过线性语言的调解。自闭症的、多层次的、精神分裂的头脑，对执着的、梦幻的、难以控制的、自白的、超自然的和极端的作出反应。一切都被1和0简化为可塑性。身份变成了多重的，而不是单一的。你是拟态传染的产物，被欲望的载体切割。感染不是诱惑，感染不是引诱，而是重新编码。就像交感魔法，就像音乐，它影响的是肉体和非物质空间的融合。

通信生成了欲望机器之间的模式识别。

后人类的愿景。音乐带你到那里。跟随声音的孤子。物种改变。机器视觉：不仅仅是一种视觉技术，而是将机器智能内部的体验建模，类似于药物感知的迷离和同时进行的复杂。它让你处于不断被放大的当下，脱离了世俗的时间，脱离了自我意识。每秒钟50万个纹理可映射的光源多边形。机器时间。数据域低语。一连串的反射，水银之上的水流过铬。身体表面成为磁化的流体。

**Fictions：**我们的工作信念是，我们所感知的真实是由虚拟和真实的混合物组成的，我们试图揭露这种电路的工作原理。我们使用各种虚构来象征顽固的文化模式，并赋予它们名称和形式(avatar)。她们的操作就像骗子一样，因为她们看起来很容易被操纵，但实际上却利用了你内心的未知。虚构就成了催化剂，让我们和观众都有了身临其境的体验。我们希望观众无法确定空间的边界及其与真实的关系：让她们在技术与谣言、信息与神秘之间做出反应，而这些都是在我们的电子媒介环境中扩散的。

*0rphan Drift*在1999年开发了5个虚拟化身，她们在主题上一直伴随着我们。

***Katak***：摩擦、残暴、辐射的人工元素。熔毁。发热、疯狂、暴行。冲击太阳波。大灾变的汇聚。压迫太阳热电。大灾变的汇聚大灾变收敛。压力太阳热电。DNA操蛋。逃逸速度。平均对称性。狂热。度过的人工夜晚。原子呼吸。

***Spl/ce***：游牧的战争机器。时间危机和不连续循环。复制不稳定的状态。群。吮吸半径：呼出精神错乱。时间分解的(chronodisintegration) 频闪的黑团(black-mass) 。不稳定的戏剧性冲击。无线性。绑架。双轨通过创伤性解离（同时不断地加倍回溯）。机器记忆深入内部。原子粒子的失去联系。

***Xes***：色情、磁力突变、流量波、细胞辐射、物理心灵感应和欺骗性触摸、上瘾、用迷幻和痛苦诱惑、你想要的代码、进化的风险因素、intimatter交通、第三只眼的疤痕、硅/克隆/迷宫。

亮片/出神/注意。***Murmur***：液态伪装。湿金属。等待。弯曲的光。弯曲的时间。变形人。脊椎液。以梦和水银为食。身临其境的编织，海洋的感觉。合并，制造怪物和波浪。

***Uttunul/IIs***：零强度平线。影子发光。亡灵。奇怪的吸引力。时间吸血鬼。以时间而非血液为食。最远的地方和最深的内部。低温反馈渗水回路。沸腾的虚空。彻底的赎罪。无器官的身体。

我们的小说是艺术创作的孪生兄弟，因为两者都是我们与机器互动的反馈。我们创造的小说涉及振动、磁场、其他维度的实体。它们触发了与虚拟现实和旧的文化生产形式相关的技术表现，如*Vodou*，一部分是操蛋都市，一部分是科幻。并强调了作品和虚构之间的增殖回路。它们各自都有众多的伪装，从可识别的人形（或标志性的），到抽象的（如声音的质地、节奏和速度），再到元素（如流动的河流或熔化的金属）。

正是这种“人工”和“真实”之间的交易，促使我们对*Vodou*和其他非洲起源的仪式传统的方法感兴趣。这些集体实践模拟了虚拟，因为它与物理世界相互作用。巫毒(*Vodou*) 的*Loa*（精神）被认为是可以暂时居住在活体上的祖先。她们每个人都保持着自己的特点和领域，同时也随着时间和环境的变化而变化。这种灵活性确保了她们总是反映当代的需求和愿望，而不是维持一个固定的理想。巫毒作为一个主题在许多关于网络空间和信息技术的文献中反复出现，因为许多新的科学和工程系统以响应为导向的模式工作，显著地改变了以前的主体/客体关系的概念。巫毒附身把身体当作一个节奏性的过程，接受古代或未来的实体。这些虚拟的非人类机构通过节奏打开人体进入更宽广的时间回路。

在外部淹没。神经质的离散重复。物质拥有虚拟的存在区域。

图样之图样。嫁接。伸展。加速。过程。强度。表面。谵妄。液体。物质。现实生产滑入。人类无法专注于如此有生命的东西。

音乐产生迷因。它的代码理解文字和潜能的交汇点。代码程序改变。代码制造漏洞。未来的钩子。泄漏。复制。代码是接口。重新编程物质形式。

你怎样记得？你能记得吗？在人类想象力结构的同化之外，还有其他类型的记忆。你是一个不稳定的空白记忆。你是一个岌岌可危的空白记忆，被置于屈服的境地。被黑的DNA是动态系统的接口，智能潜入自己的物质性，触摸，反应出身体，伸手解开，一个身体两个速度在逃逸速度中搜索。

燃烧的数据粒子在细节中导航。复杂性。频率。强度。表现线。这些声音。就像外面的旋风一样无动于衷。模型强度。颠覆性和潜意识。慢速输送代码。侵入核心。反社会和暗示。暗示：深入或密切进入物质。细胞。信息脑传播编织。看不见但继续寻找。

加速与抽象的系统碰撞。机器读取你的进程。将机器记忆深植于体内。机器面部深植于体内。原子界面。嵌入皮肤下的阿凡达外星人。通过。触摸。丝线芯片的复杂性取代了厚重缓慢的逻辑空间。突变。匹配。顺着脊柱。进入有线静止状态。将一切都记录为振动。是一种入侵而不是行为的获取。 振动是在原子水平上解开的能力，它融化了事物之间的界限，它把事物融化成界限，振动是运动的潜力，振动是变化的技术，振动总是发生在物质上，它是它的反应状态，它在物质的微观层面上触及，它产生混响，它是每一个系统的核心，每一个有机体，每一台机器，它把变化物质化，不稳定的原子在一定的频率下改变状态。流体海啸。病毒的平面。移位。编码相位转换。蜕变的人类。共振的纤维脉动进入DNA的二进制密码子。分子体已经在两种速度之间移动信息。光和声音。它是共鸣的矩阵。回响。再主动发声的质量。它始终是一个回路。信息在混响的频率中。持续频率的高低决定了什么物质受到影响。什么模式匹配。一个新的物种。作为接口。一个促进者。它的辖域才是真实的。潜行着出现。

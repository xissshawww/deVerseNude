---
title: 'Balilla Pratella :: Manifesto of Futurist Musicians'
tags:
  - Futurism/italy
date: 2021-06-01
description: "Musica futurista per orchestre riduzione per pianoforte, 1912."
---
[Musica futurista per orchestre riduzione per pianoforte, 1912.](https://www.unknown.nu/futurism/musicians.html)

I appeal to the *young.* Only they should listen, and only they can understand what I have to say. Some people are born old, slobbering spectres of the past, cryptograms swollen with poison. To them no words or ideas, but a single injunction: *the end.*

I appeal to the *young,* to those who are thirsty for the new, the actual, the lively. They follow me, faithful and fearless, along the roads of the future, gloriously preceded by my, by our, intrepid brothers, the Futurist poets and painters, beautiful with violence, daring with rebellion, and luminous with the animation of genius.

A year has passed since a jury composed of Pietro Mascagni, Giacomo Orefice, Guglielmo Mattioli, Rodolfo Ferrari and the critic Gian Battista Nappi announced that my musical Futurist work entitled *La Sina d’Vargöun,* based on a free verse poem, also by me, had won a prize of 10,000 lire against all other contenders. This prize was to cover the cost of performance of the work thus recognized as superior and worthy, according to the bequest of the Bolognese, Cincinnato Baruzzi.

The performance, which took place in December l909, in the Teatro Comunale in Bologna, brought with it success in the form of enthusiasm, base and stupid criticisms, generous defense on the part of friends and strangers, respect and imitation from my enemies. 

After such a triumphal entry into Italian musical society and after establishing contact with the public, publishers and critics, I was able to judge with supreme serenity the intellectual mediocrity, commercial baseness and misoneism that reduce Italian music to a unique and almost unvarying form of vulgar melodrama, an absolute result of which is our inferiority when compared to the Futurist evolution of music in other countries.

In Germany, after the glorious and revolutionary era dominated by the sublime genius of Wagner, Richard Strauss almost elevated the baroque style of instrumentation into an essential form of art, and although he cannot hide the aridity, commercialism and banality of his spirit with harmonic affectations and skillful, complicated and ostentatious acoustics, he nevertheless does struggle to combat and overcome the past with innovatory talent.

In France, Claude Debussy, a profoundly subjective artist and more a literary man than a musician, swims in a diaphanous and calm lake of tenuous, delicate, clear blue and constantly transparent harmonies. He presents instrumental symbolism and a monotonous polyphony of harmonic sensations conveyed through a scale of whole tones—a new system, but a system nevertheless, and consequently a voluntary limitation. But even with these devices he is not always able to mask the scanty value of his one-sided themes and rhythms and his almost total lack of ideological development. This development consists, as far as he is concerned, in the primitive and infantile periodic repetition of a short and poor theme, or in rhythmic, monotonous and vague progressions. Having returned in his operatic formulae to the stale concepts of Florentine chamber music which gave birth to melodrama in the seventeenth century, he has still not yet succeeded in completely reforming the music drama of his country. Nevertheless, he more than any other fights the past valiantly and there are many points at which he overcomes it. Stronger than Debussy in ideas, but musically inferior, is G. Charpentier.

In England, Edward Elgar is cooperating with our efforts to destroy the past by pitting his will to amplify classical symphonic forms, seeking richer ways of thematic development and multiform variations on a single theme. Moreover, he directs his energy not merely to the exuberant variety of the instruments, but to the variety of their combinational effects, which is in keeping with our complex sensibility.

In Russia, Modeste Mussorgsky, renewed by the spirit of Nikolai Rimsky-Korsakov, grafts the primitive national element on to the formulae inherited from others, and by seeking dramatic truth and harmonic liberty he abandons tradition and consigns it to oblivion. Alexander Glazunov is moving in the same direction, although still primitive and far from a pure and balanced concept of art. 

In Finland and Sweden, also, innovatory tendencies are being nourished by means of national musical and poetical elements, and the works of Sibelius confirm this.

And in Italy?

The vegetating schools, conservatories and academies act as snares for youth and art alike. In these hot-beds of impotence, masters and professors, illustrious deficients, perpetuate traditionalism and combat any effort to widen the musical field.

The result is prudent repression and restriction of any free and daring tendency; constant mortification of impetuous intelligence; unconditioned propping-up of imitative and incestuous mediocrity; prostitution of the great glories of the music of the past, used as insidious arms of offense against budding talent; limitation of study to a useless form of acrobatics floundering in the perpetual last throes of a behindhand culture that is already dead. 

The young musical talents stagnating in the conservatories have their eyes fixed on the fascinating mirage of opera under the protection of the big publishing houses. Most of them end up bad—and all the worse for lack of ideological and technical foundations. Very few get so far as to see their work staged, and most of these pay out money to secure venal and ephemeral successes, or polite toleration.

Pure symphony, the last refuge, harbors the failed opera composers, who justify themselves by preaching the death of the music drama as an absurd and anti-musical form. On the other hand they confirm the traditional claim that the Italians are not born equipped for the symphony, revealing themselves equally inept in this most noble and vital form of composition. The cause of their double failure is unique, and is not to be sought in the completely guiltless and incessantly slandered forms of opera and symphony, but in the writers’ own impotence. 

They make use, in their ascent to fame, of that absurd swindle that is called *well-made music,* the falsification of all that is true and great, a worthless copy sold to a public that lets itself be cheated by its own free will.

But the rare fortunates who, through multiple renunciations, have managed to obtain the protection of the large publishers, to whom they are tied by illusory and humiliating *noose-contracts,* these represent the classes of serfs, cowards and those who voluntarily sell themselves.

The great publisher-merchants rule over everything; they impose commercial limitations on operatic forms, proclaiming which models are not to be excelled, unsurpassable: the base, rickety and vulgar operas of Giacomo Puccini and Umberto Giordano. 

Publishers pay poets to waste their time and intelligence in concocting and seasoning—in accordance with the recipes of that grotesque pastry cook called Luigi Illica—that fetid cake that goes by the name of opera libretto.

Publishers discard any opera that surpasses mediocrity, since they have a monopoly to disseminate and exploit their wares and defend the field of action from any dreaded attempt at rebellion. 

Publishers assume protection and power over public taste, and, with the complicity of the critics, they evoke as example or warning amidst the tears and general chaos, our alleged Italian monopoly of melody and of *bel canto,* and our never sufficiently praised opera, that heavy and suffocating crop of our nation. 

Only Pietro Mascagni, the publishers’ favorite, has had the spirit and power to rebel against the traditions of art, against publishers and the deceived and spoilt public. His personal example, first and unique in Italy, has unmasked the infamy of publishing monopolies and the venality of the critics. He has hastened the hour of our liberation from commercial czarism and dilettantism in music; Pietro Mascagni has shown great talent in his real attempts at innovation in the harmonic and lyrical aspects of opera, even though he has not yet succeeded in freeing himself from traditional forms. 

The shame and filth that I have denounced in general terms faithfully represent Italy’s past in its relationship with art and with the customs of today: industry of the dead, cult of cemeteries, parching of the vital sources. 

Futurism, the rebellion of the life of intuition and feeling, quivering and impetuous spring, declares inexorable war on doctrines, individuals and works that repeat, prolong or exalt the past at the expense of the future. It proclaims the conquest of amoral liberty, of action, conscience and imagination. It proclaims that Art is *disinterest, heroism and contempt for easy success.*

I unfurl to the freedom of air and sun the red flag of Futurism, calling to its flaming symbol such young composers as have hearts to love and fight, minds to conceive, and brows free of cowardice. And I shout with joy at feeling myself unfettered from all the chains of tradition, doubt, opportunism and vanity.

I, who repudiate the title of *Maestro* as a stigma of mediocrity and ignorance, hereby confirm my enthusiastic adhesion to Futurism, offering to the young, the bold and the reckless these my irrevocable conclusions:

1. To convince young composers to desert schools, conservatories and musical academies, and to consider free study as the only means of regeneration.
2. To combat the venal and ignorant critics with assiduous contempt, liberating the public from the pernicious effects of their writings. 
    To found with this aim in view a musical review that will be independent and resolutely opposed to the criteria of conservatory professors and to those of the debased public.
3. To abstain from participating in any competition with the customary closed envelopes and related admission charges, denouncing all mystifications publicly, and unmasking the incompetence of juries, which are generally composed of fools and impotents.
4. To keep at a distance from commercial or academic circles, despising them, and preferring a modest life to bountiful earnings acquired by selling art.
5. The liberation of individual musical sensibility from all imitation or influence of the past, feeling and singing with the spirit open to the future, drawing inspiration and aesthetics from nature, through all the human and extra-human phenomena present in it. Exalting the man-symbol everlastingly renewed by the varied aspects of modern life and its infinity of intimate relationships with nature.
6. To destroy the prejudice for “well-made” music—rhetoric and impotence—to proclaim the unique concept of Futurist music, as absolutely different from music to date, and so to shape in Italy a Futurist musical taste, destroying doctrinaire, academic and soporific values, declaring the phrase “let us return to the old masters” to be hateful, stupid and vile. 
7. To proclaim that the reign of the singer must end, and that the importance of the singer in relation to a work of art is the equivalent of the importance of an instrument in the orchestra.
8. To transform the title and value of the “operatic libretto” into the title and value of “dramatic or tragic poem for music”, substituting free verse for metric structure. Every opera writer must absolutely and necessarily be the author of his own poem.
9. To combat categorically all historical reconstructions and traditional stage sets and to declare the stupidity of the contempt felt for contemporary dress. 
10. To combat the type of ballad written by Tosti and Costa, nauseating Neapolitan songs and sacred music which, having no longer any reason to exist, given the breakdown of faith, has become the exclusive monopoly of impotent conservatory directors and a few incomplete priests. 
11. To provoke in the public an ever-growing hostility towards the exhumation of old works which prevents the appearance of innovators, to encourage the support and exaltation of everything in music that appears original and revolutionary, and to consider as an honor the insults and ironies of moribunds and opportunists.

And now the reactions of the traditionalists are poured on my head in all their fury. I laugh serenely and care not a jot; I have climbed beyond the past, and I loudly summon young musicians to the flag of Futurism which, [launched by the poet Marinetti in *Le Figaro* in Paris, ](https://www.unknown.nu/futurism/manifesto.html)has in a short space of time conquered most of the intellectual centers of the world.

---
title: '🔮 Les Rallizes Dénudés Archive Project'
date: 2021-02-05
tags: 
  - les-rallizes-denudes
description: "We are calling for a Les Rallizes Dénudés Archive!!!"
---

![](lrd.jpg)

已经有日本网友[ubud](http://rallizes.blogspot.com/search/label/about)做了一个裸身集会的资料blog：[The Last One blogspot.com](http://rallizes.blogspot.com/)（感谢ubud，当年开始狂热裸集的时候在ubud的这个blog中学习到了很多关于lrd的历史，这是目前资料最全面的裸集资料blog）

## 为了让裸身集会在20年代更加鲜活，这个计划：

1. 比起简单的资料archive更加灵活的有创造力的manifesto & archive。
2. 静态，非静态放在两个站点
3. 文字资料都可以一键打包（比如[https://cyberfeminismindex.com](https://cyberfeminismindex.com/)）
4. 设置提交（网友的）私有资料的窗口；设置留言板；

## 初步要向[rallizer们](https://www.douban.com/group/mizutani/)征集的意见：

1. 网站的中英日文命名（不想直接使用lrd archive这样的命名方式）及域名
2. 内容、分类
3. 配色、logo
4. 以及一切跟此（将要来到的）网站有关的东西

------------

## 开发日志
```md
2021.3.31 
1. 完成demo1

2021.4.7
1. 准备购买VPS
2. 使用tiddlywiki做一个lrdwiki
3. 准备做demo2，风格参照 [cyberfeminismindex](https://cyberfeminismindex.com/) 和 [amkanngieser](https://amkanngieser.com/)

2021.5.31
1. 完成主站[LRDA](deversnude.xyz/kaos/indoom.html)
（名字待定，名字不是LRDA）
（url将会被重置）

2021.6.1
1. 完成实验站点[flame to come](deversnude.xyz/kaos/fire--to--come/)
（将会放在另一个服务器上，重置域名）
2. 计划修改为更加平易近人的黑暗风格

```

------------

如果你有任何想法或者任何可以免费提供的支持请[DM我](https://t.me/nonxishaw)！

------------

地下日音俱乐部&唱片行[空之穴（Hole In the Sky）](https://weidian.com/?userid=272667841&wfr=c&ifr=itemdetail&source=home_shop&sfr=app)的[阿一](https://www.douban.com/people/cristianosick/)愿意分享号里有关lrd的（精彩）内容到这个Archive，真的太感谢

空之穴有一些很棒的关于lrd的内容：

* 饭团翻译的：[特辑 裸のラリーズ （一）（二）](https://mp.weixin.qq.com/s/xmV0xQ857dtN9zu20P4b0A)
* [Nene](https://ningkko.github.io/)翻译的：[想要知道更多的裸のラリーズ（水谷孝×汤浅学fax通信）](https://mp.weixin.qq.com/s/OKTOBSgpq2jHqBxEsx0-kQ)
* 「ロック画報」25的LRD专刊的翻译连载。

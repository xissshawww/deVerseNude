---
title: '🔮 LRDA'
date: 2021-06-03
tags: 
  - les-rallizes-denudes
description: "网站上线了，中短期内不会进行宣传"
---

进入这个子站的方式是点击右上角的水晶球🔮。
目前要说的话参见：[love](https://deversnude.xyz/kaos/love.html)

## 开发日志
```md
2021.3.31 
1. 完成demo1

2021.4.7
1. 准备购买VPS
2. 使用tiddlywiki做一个lrdwiki
3. 准备做demo2，风格参照 [cyberfeminismindex](https://cyberfeminismindex.com/) 和 [amkanngieser](https://amkanngieser.com/)

2021.5.31
1. 完成主站[LRDA](https://deversnude.xyz/kaos/indoom.html)
（名字待定，名字不是LRDA）
（url将会被重置）

2021.6.1
1. 完成实验站点[flame to come](https://deversnude.xyz/kaos/fire--to--come/)
（将会放在另一个服务器上，重置域名）
2. 计划修改为更加平易近人的黑暗风格

```

---
title: "《音乐物件论—— 一篇跨学科论文》序言"
date: 2022-04-16T19:19:05+08:00
draft: false
tags: [Pierre Schaeffer, Sound Objects]
description: '“我们的国度可不属于这个世界。”音乐家们说。'
---

本文授权转载自 [尘鉴](https://mp.weixin.qq.com/s/gGZH1KKzIZgfJih-ANAWog)，译者徐程。

> 本文为Pierre Schaeffer的《音乐物件论—— 一篇跨学科论文》（Treatise on Musical Objects —— An Essay across Disciplines）的序言部分(pp. xxxvii)，译自Christine North和John Dack 的英文译本，原书共七册，每册针对不同的角度和问题，序言里也稍有涉及。本人翻译水准拙劣，且Schaeffer行文诗意，原文本就有难以翻译的说法，再则译自英版，权当共享我的自学笔记吧。虽如此仍然欢迎大家指正纠错，毕竟这仍是具象音乐非常重要的一手概念资料。会否继续往下继续译，全凭天意。文中的Musical Object和Sound Object是Schaeffer特有的概念，并没有很好的翻译方法。我之前一般把Sound Object译成“声音体”，这里沿用姚大钧老师“声音物件”的译法。末尾附带 INA-GRM Solfège de l'objet sonore (Schaeffer 1967) 英字片段视频以供参考。 
> 
> 2021春 徐程

> ![](book.jpg)

## 序言

“我们的国度可不属于这个世界。”音乐家们说。

> 因为，我们在哪儿能像画家和雕塑家那样在自然中找到我们艺术的原型呢？声音栖身于万物，但声音，我指讲着精神王国更高等语音的旋律，却只栖身于人类心中。然而，音乐的精神，不就像声音的精神一样，包容着整个自然界吗？声之体，被机械地触动、觉醒、显现其存在或更确切地说，显现其结构，从而进入我们的意识。那音乐的精神又如何呢？是否在其觉醒后用神秘的和弦以和鸣和旋律的方式、用唯其自明的方式表达着自我呢？于是，当音乐家灵感忽临，旋律生于其心内，是对自然秘密之乐的觉知，或是无意识，不可言说的概念，即是生命或所有生命活动的原则。因此，音乐家难道不能与催眠师、梦游者那样，站在与自然界相等的关系中吗？ 
> ......听觉即是内观......” [^1]

霍夫曼的文字像他的《克莱斯勒里亚纳》（Kreisleriana）写作后的150年一样，轻描淡写地承载着今天乃至永远的问题。但浪漫主义存着我们必须摆脱的高傲姿态：纯真而慷慨、才华横溢而矜持。试问我们中，甚至其他时代中，何人敢以此方式来审问音乐？

然而，这样的勇气激励着我们。即使我们不再以自然之名来召唤它，因为它现在被称为科学（Science）了，即使精神（spirit），开头不用大写字母，且在它的所有机制中被当作知识现象的器具来检验，我们也很难说得更清楚。知识增加；实验越来越多；调查领域越来越大，越来越零碎。只有在表面上，技艺与技术学（technique and technology）、乐器学（Instrumentarium）[^2]与声学、音乐理论[^3]与作曲、心理学与音乐学、音乐文明史和我们自己，这些不同领域之间建立了关系。而此辽阔的土地上的裂痕也许更深了，因为它不那么明显、伪装得更好。

整合的挑战始终是诱人的，而思分两路：一，认为知识的积累提供了解决方案，最终音乐将被放入一个方程；二，精细的思维推导反而导致了简单的问题，而科学的方程和艺术的直觉都建立在这问题之上。

几乎没有哪个哲学家或真正的学者，在哪个学科、哪个时候提出过李维斯陀这样的想法：“也许有一天，我们会发现在神话和科学思维中，人类永远在用同样的方式在思考。思维进程（Progress）「如果这个词仍然适用的话」不会以意识为其领域，但在这个被赋予人性的、派系永存的世界上，在其漫长的历史过程中会不断地与新的对象发生联系。”[^4]

从这个角度看，音乐将为研究和审视提供一种独特的机会。事实上，在任何领域中，在任何其他语言中也一样，在选择和组合对象的方式看起来是如此自由的前提下，给定的对象却又如此精确。因此，音乐在其进程中可以被看作是与科学进程相联系的——从声学，以及现在从电子学、电子声学中汲取手法。在有了这么多新的 _声音物件_ _（sound objects）_ 后，应有可能辨别出人类思维和感性的常态结构。然后我们便会发现，在这种自然赋予的手段和文化结构的互补中，许多表面矛盾，如古代和现代，艺术和科学，声音和音乐之间得到了解决方案。这正是霍夫曼所梦想的精神与自然之间的对话。

本书的整个计划就是受这种二元论的启发。它的目的是涵盖不断增长的 _声音物件_ _（sound objects）_ 领域，并看到音乐结构是如何从现象中衍生出来的，其更普遍的规律，它们只是在一个非常重要的特殊情况下验证了这些规律。因此，从一门学科到另一门学科，应该可以找到缺失的联系——不是基于物理涵义或文学类比，那种粗糙、脆弱的关联，而是基于我们所期待发现其原始机制的横向关系。

然而，即使有了这双重目标，要分裂我们的思维过程也不是那么容易的，我们的叙述过程也只能一次性完成；更何况，即使它的目的是要把各种类别的思维和能力汇集在一起，它也希望用最熟悉的东西来吸引其中的每一个人，除非读者们通过其探索精神，我们也乐于你们具有此特质，来转向他最不熟悉的学科。因此，这曲折的思维过程就像是多级跳：旨在从当前的关于制造音乐和听觉的信息（第1及第2册）转向两个更具体的启发的两个冥想，一个由物理学启发（第3册），另一个由哲学启发（第4册）。相较列出这些文字，我们更应去重新思考。实际上这后两册，触及了它们各自学科的主题，但我们的主要目的是藉这些学科前沿来一窥音乐领域中尚鲜为人知的地带。所以，我们倡议所有人都应去关注那些不是用这领域自身领域的语言写就的叙述。

最后但同样重要的是，音乐家应感安心，因为这整部作品主要是为他而作的。但是，他不应该期望在这里有一个 “音乐理论”：它仅涉及到 _“音乐物件”_ _（musical object）_ 的实践。即使音乐理论对于方便地表达音乐创作的问题是必不可少的，我们也不必应允它去越界宣示要解决艺术构成（composition）本身的问题。阅读本书的音乐家，如果急于得到的声音形态学和音乐理论方面的知识，可以快速浏览本书的第三和第四册，但请确保读完本书并能一窥本书的基本原理。也许那时他将认为，这些部分值得重新审视。在这些在哲学和科学所提出的努力需求是公正的；当代音乐常常过于天真地偏离其中之一或另一个领域，我们却无法在这种肤浅的方法中找到满足感。

至于最后一册，它的和之前的几册的笔调不太一样。作者承认，他允许自己在这里更多地以个人名义说话。因此，他不会责怪读者没有跟随他的结论走到底。但他并不希望读者被这些结论所冒犯，并能冷静地研读前几册：这些书是近二十年来为了使各个学科相互直面而进行的实验性工作的成果。

当一个人长年累月地从事这样一项基础性的研究，而这项研究在许多方面似乎也是一门原创性的学科时，向公众展示它的合适时机似乎从未到来。即使出发点是充满希望的，方法是连贯的，但从一个阶段到另一个阶段，人们首先了解到的是自己知道的是多么的少，整个研究事业是多么的庞大。但是，这样一来，我们就没有理由发表文章了，而且我们会发现自己的处境与我们的方法完全相反，因此我们的方法是以成立研究小组为前提的。

我们清楚地知道，这种做法与当代的惯例大相径庭：任何人都不应该毫无顾忌地在自己的专业领域内发表文章。而当我们发现所有这些专业领域都连在一起的时候，我们实在是被解除了武装的。在解开这最多样化的、往往也是最多分歧的学科的死结时，我们被它们团团困住。这场不公平的战斗：库里亚特三胞胎[^5]在同时袭击我们，各个健壮，而我们很快就露出疲态。如果我们带着橄榄枝（免战牌）就好了! 但是，作者秉性使然，已给自己武装上了能使专家们打喷嚏的粉药。

还有一个迫使我们完成此书的原因。这本书，这个团队合作的成果，是现在期盼能运用我们成果的人和希望从事这项工作的人所不可缺少的资料。这项整合工作的最终结果，不单单显现出作者的职责，但它也有赖于许多相关工作和整个小组的合作。自1950年这项原始研究之始，情况就是如此。技术想象部分来自Jacques Poullin和Francis Coupigny[^6]，而音乐实验是一种“连锁反应”部分和Pierre Henry、Luc Ferrari和François Bayle[^7]紧紧关联。最近，Guy Reibel和Enrico Chiarucci[^8]在声学领域进行了纪录和实验性的尝试。最后，编辑工作是由Pierre Janin和Sophie Brunet[^9]共同完成的，后者尚不止此贡献。我向他们所有人表示衷心的感谢。

此外，同一团队还在进行一项补充工作，其成果将与本著作同时以若干黑胶唱片的形式出版：它是文本的声音对应物，是不可或缺的听觉材料，有了它才能使读者从概念走向感知。

最后，不得不提法国广播电视局(ORTF)的一贯支持，从1948年Wladimir Porché的第一句，到Jacques-Bernard Dupont[^10]的所有鼓励，该局一直慷慨地支持着这项奇怪的研究。

1966年8月14日，巴黎

皮耶 沙费

---

[Δ Solfège de l'objet sonore (Schaeffer 1967) CD 1 Tracks 1-11](cd1.mp4)  
[Δ Solfège de l'objet sonore (Schaeffer 1967) CD 2 Tracks 27-37](cd2.mp4)

---

[^1]: E. T. A. Hoffmann, _E. T. A Hoffmann’s Musical Writings: “Kreisleriana,” “The Poet and the Composer,” Music Criticism_, ed. David Charlton, trans. Martyn Clarke (Cambridge: Cambridge University Press, 1989), 74, 163–64.
[^2]: The word used in French is _lutherie_, which means the craftsmanship of instrument making but which Schaeffer used in a broader connotation to refer to virtual instruments as well. _Instrumentarium_, we hope, captures this double meaning, although where the context is appropriate, _lutherie_ is translated simply as “instrument making.”—Trans. 

[^3]: The word used in French is _solfège_, a word occurring in romance languages but that does not have an English equivalent. _Sol-fa_ is sometimes used in the United Kingdom or _Ear-training_ in the United States. _Music theory_ is mainly used when referring to _solfège_, although occasionally, where the context demands this, it is translated as “musicianship.”—Trans. 

[^4]: Claude Lévi-Strauss, _Anthropologie structurale_ (Paris: Plon, 1958), 254. Published in English as _Structural Anthropology_ (New York: Basic Books, 1963).—Trans.
[^5]: According to the Roman historian Livy(50BCE-CE17).The Curiatii and the Horatii were male triplets from Alba Longa and Rome, who fought to settle the war between the two cities during the reign of Tullus Hostilius (672–642 BCE). The Horatii finally overcame the Curiatii.—Trans.
[^6]: Jacques Poullin and Francis Coupigny were Schaeffer’s chief engineers from 1950 to 1975. They contributed significantly to the invention and development of new tools for sound processing.—Trans.
[^7]: Pierre Henry was Schaeffer’s first collaborator in his “musique concrète” compositions and later became one of the great figures of electroacoustic music; Luc Ferrari and François Bayle are two major composers who worked at the Groupe de recherches musicales founded by Schaeffer in 1958. Bayle became director of the group in 1966.—Trans.  
[^8]: Guy Reibel and Enrico Chiarucci, both composers working at the GRM in the 1960s, wrote and developed a book of sound examples, the _Solfège de l’objet sonore_, in 1967, which is a guide for the understanding of the _Treatise on Musical Objects_. In 1998 the _Solfège de l’objet sonore_ was reissued by the Ina/GRM as a booklet with 3 CDs.—Trans.
[^9]: Two close collaborators of Pierre Schaeffer.—Trans.
[^10]: Wladimir Porché was the head of the RTF (Radiodiffusion-télévision française) in 1951 when Pierre Schaeffer, after starting his experiments on “musique concrète,” created the Groupe de recherche de musique concrète (GRMC); Porché supported this experimental approach and provided the infrastruc- ture for Schaeffer’s work. Jacques-Bernard Dupont was the head of the ORTF (Office de radiodiffusion- télévision française) when Schaeffer published the Treatise on Musical Objects in 1966. —Trans.

![](schaeffer.jpg)

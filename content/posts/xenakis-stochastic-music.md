---
title: "Xenakis :: Stochastic Music"
date: 2021-12-06T10:30:35+08:00
tags: [lannis Xenakis, Stochastic Music]
category: [Computer Music]
# bookComments: false
# bookSearchExclude: false
---
> 究竟何謂謝納奇斯所謂之隨機音樂？謝納奇斯為一般無法直接閱讀其數學方程式之讀者舉出了現實生活之中的例子:
> 自然現象，諸如群衆呼喊、下雨，夏日野地群蟬齊嗚等等，諸如此類的聲晉現象其實是千萬僙單獨聲音組合而成，但當它們於同一時空發生，便產生了異於其個別表現的、一種全新的聲晉。這種音團現象是時間的适形，遵循隨機(aleatoire et stochastic)的原則，如果某人要製造像這樣由大量點狀晉、例如撥弦音(pizzicati)這樣的短促音所 組成的音群，就只能夠以數學 方法從事創作，亦即一種簡練、緊湊目合乎邏輯之理性表違方式。大家都聽過政治示威追行中，數萬乃至數十萬群衆所一同發出的聲晉;群衆所構成的人蘢以統一的節奏呼喊口號，當下一句口號田身處群衆前端的指撣者發出寸更如同波浪一般由前向後推迤，擠走上一波口號。當整個城市充滿暄嚷，鎮蜃者的聲昔與節秦隨後出現，出現了充滿殘暴力量的奇異場景。當示威者與反示威者之肢體衝突開始，示威者們最後一句整齊的口號為無序的呼喊晉團所打散，吼叫聲如波浪傳到人龍尾部，想像此時，數十挺機槍開始吐火，子彈狂嘯劃空，為這聲陣渾沌劃上標點，人龍潰散，地獄般的場景與聲櫺隨之出現，寂靜如爆炸般接管了其後絶望、髒污、死亡的局面。撇開政治與道德的脈絡不論，造成此種人間場景背後的數學(統計學)法則，其實與一場大雨或者夏天原野上群蟬爭嶋並無不同，此種漸進的或爆炸般 地從有序轉為無序的過程，都是隨機法則(loi stochastic)的應用。(註18 lannis Xenakis. Formalized Music. New York: Pendragon Press, 1992, p.9) #xenakis
> 
>
> natural events such as the collision ofhail or rain with hard surfaces, or the song of cicadas in a summer field. These sonic events are made out of thou- sands ofisolated sounds; this multitude ofsounds, seen as a totality, is a new sonic event. This mass event is articulated and forms a plastic mold of time, which itselffollows aleatory and stochastic laws. Ifone then wishes to form a large mass of point-notes, such as string pizzicati, one must know these mathematical laws, which, in any case, are no more than a tight and concise expression of chain of logical reasoning. Everyone has observed the sonic phenomena of a political crowd of dozens or hundreds of thousands of people. The human river shouts a slogan in a uniform rhythm. Then another slogan springs from the head of the demonstration; it spreads towards the tail, replacing the first. A wave oftransition thus passes from the head to the tail. The clamor fills the city, and the inhibiting force of voice and rhythm reaches a climax. It is an event of great power and beauty in its ferocity. Then the impact between the demonstrators and the enemy occurs. The perfect rhythm of the last slogan breaks up in a huge cluster of chaotic shouts, which also sprcads to the tail. Imagine, in addition, the reports of dozens of machine guns and the whistle of bullets addipg their punctuations to this total disorder. The crowd is then rapidly dispersed, and after sonic and visual hell follows a detonating calm, full of despair, dust, and death. The statistical laws of these events, separated from their political or moral contcxt, are the same as those ofthc cicadas or the rain. Thcy are the laws of the passage from complete order to total disorder in a continuous or explo- sive manner. They are stochastic laws. #xenakis

摘自超越声音艺术 P75
